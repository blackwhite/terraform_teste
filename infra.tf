provider "digitalocean" {
    token = "${var.do_token}"
}

resource "digitalocean_droplet" "web" {
    image = "ubuntu-14-04-x64"
    name = "web-1"
    region = "nyc3"
    size = "512mb"
}

resource "digitalocean_ssh_key" "default" {
    name = "Terraform"
    public_key = "${file(var.ssh_key_path)}"
}
